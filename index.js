const child_process = require('child_process');
const process = require('process');

module.exports = {

    run: function (target, done, err, progress) {
        
        let proc = child_process.spawn('traceroute', [target]);

        proc.stdout.on('data', (data) => {
            progress(data.toString());
        });

        proc.stderr.on('data', (data) => {
            err(data.toString());
        });

        proc.on('close', (code) => {
            if(code === null)
                done(-1);
            else
                done(code.toString());
        });

        return proc;

    }

};

