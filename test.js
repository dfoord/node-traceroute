const process = require('process');
const traceroute = require('./index.js');

let proc = traceroute.run('google.com', (exitCode) => {
   process.stdout.write(`\ntraceroute exited with code ${exitCode}`);
}, (err) => {
   process.stdout.write(err);
}, (progress) => {
   process.stdout.write(progress);
});

console.log(proc.pid);